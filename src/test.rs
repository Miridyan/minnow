#![cfg(test)]
use crate::{sprint, sprintln};

#[derive(Debug)]
#[repr(u32)]
enum QemuExitCode {
    Success = 0x10,
    Failure = 0x11,
}

fn exit_qemu(code: QemuExitCode) {
    use x86_64::instructions::port::Port;

    unsafe {
        let mut port = Port::new(0xf4);
        port.write(code as u32);
    }
}

pub trait Testable {
    fn run(&self);
}

impl<T: Fn()> Testable for T {
    fn run(&self) {
        sprint!("{}...\t", core::any::type_name::<T>());
        self();
        sprintln!("[ok]");
    }
}

#[cfg(test)]
pub fn runner(tests: &[&dyn Testable]) {
    sprintln!("\nRunning {} tests\n", tests.len());
    tests.iter().for_each(|test| test.run());

    exit_qemu(QemuExitCode::Success);
}

#[cfg(test)]
pub fn panic(info: &core::panic::PanicInfo) {
    sprintln!("[failed]");
    sprintln!("{info}");

    exit_qemu(QemuExitCode::Failure);
}

#[test_case]
fn trivial() {
    assert_eq!(0, 0);
}
