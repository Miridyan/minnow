use core::fmt::Write;
use lazy_static::lazy_static;
use spin::Mutex;
use volatile::Volatile;


const BUFFER_HEIGHT: usize = 25;
const BUFFER_WIDTH: usize = 80;
const BUFFER_ADDR: u32 = 0xb8000;

lazy_static! {
    pub static ref WRITER: Mutex<Writer> = Mutex::new(Writer {
        column: 0,
        row: 0,
        color: Color::new(ColorCode::White, ColorCode::Black),
        buffer: unsafe { &mut *(BUFFER_ADDR as *mut Buffer) },
    });
}

#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => ($crate::vga::_print(format_args!($($arg)*)));
}

#[macro_export]
macro_rules! println {
    () => ($crate::print!("\n"));
    ($($arg:tt)*) => ($crate::print!("{}\n", format_args!($($arg)*)));
}

#[allow(dead_code)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum ColorCode {
    Black = 0,
    Blue = 1,
    Green = 2,
    Cyan = 3,
    Red = 4,
    Magenta = 5,
    Brown = 6,
    LightGray = 7,
    DarkGray = 8,
    LightBlue = 9,
    LightGreen = 10,
    LightCyan = 11,
    LightRed = 12,
    Pink = 13,
    Yellow = 14,
    White = 15,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(transparent)]
pub struct Color(u8);

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(C)]
pub struct Char {
    ascii: u8,
    color: Color,
}

#[derive(Debug)]
#[repr(transparent)]
pub struct Buffer {
    chars: [[Volatile<Char>; BUFFER_WIDTH]; BUFFER_HEIGHT],
}

#[derive(Debug)]
pub struct Writer {
    pub column: usize,
    pub row: usize,
    pub color: Color,
    pub buffer: &'static mut Buffer,
}

impl Color {
    pub const fn new(fg: ColorCode, bg: ColorCode) -> Self {
        Self((bg as u8) << 4 | (fg as u8))
    }
}

impl Writer {
    pub fn write_byte(&mut self, b: u8) {
        match b {
            b'\n' => self.new_line(),
            b => {
                if self.column >= BUFFER_WIDTH {
                    self.new_line();
                }

                let row = self.row;
                let col = self.column;

                let color = self.color;

                self.buffer.chars[row][col].write(Char {
                    ascii: b,
                    color,
                });

                self.column += 1;
            },
        }
    }

    pub fn write_string(&mut self, string: &str) {
        string.bytes().for_each(|b| match b {
            0x20..=0x7e | b'\n' => self.write_byte(b),
            b'\t' => for _ in 0..4 {
                self.write_byte(b' ');
            },
            _ => self.write_byte(0xfe),
        });
    }

    pub fn new_line(&mut self) {
        if self.row == BUFFER_HEIGHT - 1 {
            self.scroll();
        } else {
            self.row += 1;
        }

        self.column = 0;
    }

    pub fn clear_line(&mut self, row: usize) {
        if row < BUFFER_HEIGHT {
            let blank_char = Char {
                ascii: b' ',
                color: self.color,
            };

            for col in 0..BUFFER_WIDTH {
                self.buffer.chars[row][col].write(blank_char);
            }
        }
    }

    pub fn scroll(&mut self) {
        for row in 1..BUFFER_HEIGHT {
            for col in 0..BUFFER_WIDTH {
                let character = self.buffer.chars[row][col].read();
                self.buffer.chars[row - 1][col].write(character);
            }
        }

        self.clear_line(BUFFER_HEIGHT - 1);
    }

    pub fn clear_screen(&mut self) {
        for row in 0..BUFFER_HEIGHT {
            self.clear_line(row);
        }

        self.row = 0;
        self.column = 0;
    }
}

impl Write for Writer {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        self.write_string(s);
        Ok(())
    }
}

#[doc(hidden)]
pub fn _print(args: core::fmt::Arguments) {
    WRITER.lock().write_fmt(args).unwrap();
}

pub fn clear_screen() {
    WRITER.lock().clear_screen();
}
