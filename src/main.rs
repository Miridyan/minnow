#![no_std]
#![no_main]

#![feature(abi_x86_interrupt)]
#![feature(custom_test_frameworks)]

#![test_runner(crate::test::runner)]
#![reexport_test_harness_main = "test_main"]

mod bits;
mod interrupts;
mod vga;
mod serial;
mod test;

#[no_mangle]
pub extern "C" fn _start() -> ! {
    interrupts::x86_64::init();

    #[cfg(test)]
    test_main();

    #[cfg(not(test))]
    kern_main();

    loop {}
}

#[panic_handler]
fn panic(info: &core::panic::PanicInfo) -> ! {
    #[cfg(test)]
    test::panic(info);

    #[cfg(not(test))]
    kern_panic(info);

    loop {}
}

#[cfg(not(test))]
fn kern_panic(info: &core::panic::PanicInfo) {
    println!("{info}");
}

#[cfg(not(test))]
fn kern_main() {
    println!("Hello, world!");
}
