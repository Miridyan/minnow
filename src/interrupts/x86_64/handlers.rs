use crate::println;
use x86_64::structures::idt::InterruptStackFrame;

pub(super) extern "x86-interrupt" fn breakpoint(
    sf: InterruptStackFrame
) {
    println!("EXCEPTION: BREAKPOINT\n{sf:?}");
}

pub(super) extern "x86-interrupt" fn double_fault(
    sf: InterruptStackFrame,
    _: u64,
) -> ! {
    panic!("EXCEPTION: DOUBLE FAULT\n{sf:?}");
}
