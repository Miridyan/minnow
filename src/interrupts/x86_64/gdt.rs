use lazy_static::lazy_static;
use x86_64::{
    VirtAddr,
    instructions::{
        tables::load_tss,
        segmentation::{CS, Segment},
    },
    structures::{
        gdt::{Descriptor, GlobalDescriptorTable, SegmentSelector},
        tss::TaskStateSegment,
    },
};

const STACK_SIZE: usize = 4096 * 5;
pub(super) const DOUBLE_FAULT_IST_INDEX: usize = 1;

struct Selectors {
    code_segment_selector: SegmentSelector,
    tss_segment_selector: SegmentSelector,
}

lazy_static! {
    static ref TSS: TaskStateSegment = {
        let mut tss = TaskStateSegment::new();
        tss.interrupt_stack_table[DOUBLE_FAULT_IST_INDEX] = {
            static mut STACK: [u8; STACK_SIZE] = [0; STACK_SIZE];

            let start_stack = VirtAddr::from_ptr(unsafe { &STACK });
            let end_stack = start_stack + STACK_SIZE;

            end_stack
        };
        tss
    };
    static ref GDT: (GlobalDescriptorTable, Selectors) = {
        let mut gdt = GlobalDescriptorTable::new();

        let code_segment_selector = gdt.add_entry(Descriptor::kernel_code_segment());
        let tss_segment_selector = gdt.add_entry(Descriptor::tss_segment(&TSS));
        (gdt, Selectors { code_segment_selector, tss_segment_selector })
    };
}

pub(super) fn init() {
    GDT.0.load();

    unsafe {
        CS::set_reg(GDT.1.code_segment_selector);
        load_tss(GDT.1.tss_segment_selector);
    }
}
