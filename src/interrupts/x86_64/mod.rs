mod gdt;
mod handlers;

use lazy_static::lazy_static;
use x86_64::structures::idt::InterruptDescriptorTable;

#[cfg(test)]
use x86_64::instructions::interrupts;

// const STACK_SIZE: usize = 4096 * 5;
// const DOUBLE_FAULT_IST_INDEX: usize = 1;

lazy_static! {
    static ref IDT: InterruptDescriptorTable = {
        let mut idt = InterruptDescriptorTable::new();
    
        idt.breakpoint.set_handler_fn(handlers::breakpoint);
        unsafe {
            idt.double_fault.set_handler_fn(handlers::double_fault)
                .set_stack_index(gdt::DOUBLE_FAULT_IST_INDEX as u16);
        }
        idt
    };
    // static ref TSS: TaskStateSegment = {
    //     let mut tss = TaskStateSegment::new();
    //     tss.interrupt_stack_table[DOUBLE_FAULT_IST_INDEX] = {
    //         static mut STACK: [u8; STACK_SIZE] = [0; STACK_SIZE];

    //         let start_stack = VirtAddr::from_ptr(unsafe { &STACK });
    //         let end_stack = start_stack + STACK_SIZE;

    //         end_stack
    //     };
    //     tss
    // };
    // static ref GDT: GlobalDescriptorTable = {
    //     let mut gdt = GlobalDescriptorTable::new();

    //     gdt.add_entry(Descriptor::kernel_code_segment());
    //     gdt.add_entry(Descriptor::tss_segment(&TSS));
    //     gdt
    // };
}

pub fn init() {
    // GDT.load();
    gdt::init();
    IDT.load();
}

#[test_case]
fn test_breakpoint_handler() {
    interrupts::int3();
}
