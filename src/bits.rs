use core::ops::*;

pub trait OrOps<Output>: BitOr<Output> + BitOrAssign<Output> {}
pub trait AndOps<Output>: BitAnd<Output> + BitAndAssign<Output> {}
pub trait XorOps<Output>: BitXor<Output> + BitXorAssign<Output> {}
pub trait ShrOps<Output>: Shr<Output> + ShrAssign<Output> {}
pub trait ShlOps<Output>: Shl<Output> + ShlAssign<Output> {}

impl<O: BitOr<O> + BitOrAssign<O>> OrOps<O> for O {}
impl<O: BitAnd<O> + BitAndAssign<O>> AndOps<O> for O {}
impl<O: BitXor<O> + BitXorAssign<O>> XorOps<O> for O {}
impl<O: Shr<O> + ShrAssign<O>> ShrOps<O> for O {}
impl<O: Shl<O> + ShlAssign<O>> ShlOps<O> for O {}

pub trait BitField<O>: OrOps<O> + AndOps<O> + XorOps<O> + ShrOps<O> + ShlOps<O> {}

impl<O> BitField<O> for O
where
    O: OrOps<O> + AndOps<O> + XorOps<O> + ShrOps<O> + ShlOps<O> {}

pub trait Assign<O>: BitField<O> {
    fn assign(&mut self, range: Range<O>, value: O);
}

// impl<O: BitField<O>> Assign<O> for O {
//     fn assign(&mut self, range: Range<O>, value: O) {
//         
//     }
// }
